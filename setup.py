from setuptools import setup

setup(
    name='svgcolorizor',
    packages=['svgcolorizor'],
    include_package_data=True,
    install_requires=[
        'flask',
        'flask_cors',
    ],
)