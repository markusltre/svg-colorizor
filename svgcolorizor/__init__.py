from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

import svgcolorizor.views

if __name__ == '__main__':
    app.run(port=5020, debug=False)