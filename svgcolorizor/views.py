from flask import render_template
import jinja2

from svgcolorizor import app

@app.route('/<icon>')
@app.route('/<icon>/<color>')
def icon(icon, color='#000000'):
    # print('icon: '+icon+' | color: '+color)
    try:
        return render_template('icons/'+icon+'.svg', color='#'+color)
    except(jinja2.exceptions.TemplateNotFound):
        return 'icon not found'